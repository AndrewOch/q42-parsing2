package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.*;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    Map<String, String> hashMap = new HashMap<>();
    List<String> departments = new ArrayList<>();
    List<FactoryParsingException.FactoryValidationError> errors = new ArrayList<>();

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {

        File dir = new File(factoryDataDirectoryPath);
        File[] files = new File[0];
        if (dir.isDirectory()) {
            files = dir.listFiles();
        }

        assert files != null;
        Arrays.stream(files).forEach(file -> {
            try {
                FileReader fileReader = new FileReader(file);

                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String s;
                while ((s = bufferedReader.readLine()) != null) {
                    String substring = null;
                    if (s.contains(":")) {
                        substring = s.substring(s.indexOf(':') + 1);
                        if (substring.length() == 0 || substring.equals("null")) {
                            substring = null;
                        } else {
                            substring = substring.substring(substring.indexOf('"') + 1, substring.lastIndexOf('"'));
                        }
                    }
                    if (substring != null && (substring.length() == 0 || substring.equals("null"))) {
                        substring = null;
                    }
                    if (s.contains("title")) {
                        hashMap.put("title", substring);
                    }
                    if (s.contains("description")) {
                        hashMap.put("description", substring);
                    }
                    if (s.contains("firstName")) {
                        hashMap.put("firstName", substring);
                    }
                    if (s.contains("middleName")) {
                        hashMap.put("middleName", substring);
                    }
                    if (s.contains("secondName")) {
                        hashMap.put("secondName", substring);
                    }
                    if (s.contains("amountOfWorkers")) {
                        hashMap.put("amountOfWorkers", substring);
                    }
                    if (s.contains("departments")) {
                        assert substring != null;
                        String[] split = substring.split("\", \"");
                        departments.addAll(Arrays.asList(split));
                    }
                }
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return validationCheck();
    }

    public Factory validationCheck() {

        Class<?> aClass = Factory.class;
        try {
            Constructor<?> declaredConstructor = aClass.getDeclaredConstructor();

            declaredConstructor.setAccessible(true);
            Factory factory = (Factory) declaredConstructor.newInstance();
            Field[] declaredFields = aClass.getDeclaredFields();

            Arrays.stream(declaredFields).forEach(field -> {
                boolean hasAnnotations = false;
                field.setAccessible(true);
                Concatenate concatenate = field.getDeclaredAnnotation(Concatenate.class);
                if (concatenate != null) {
                    hasAnnotations = true;
                    try {
                        StringBuilder str = new StringBuilder();
                        if (concatenate.fieldNames().length > 0) {
                            str.append(hashMap.get(concatenate.fieldNames()[0]));
                            for (int i = 1; i < concatenate.fieldNames().length; i++) {
                                str.append(concatenate.delimiter()).append(hashMap.get(concatenate.fieldNames()[i]));
                            }
                        }
                        field.set(factory, str.toString());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                NotBlank notBlank = field.getDeclaredAnnotation(NotBlank.class);

                if (notBlank != null) {
                    hasAnnotations = true;
                    try {
                        if (hashMap.get(field.getName()) == null) {
                            errors.add(new FactoryParsingException.FactoryValidationError(field.getName(), "Поле не должно быть пустым!"));
                        } else {
                            if (field.getType() == LocalDate.class) {
                                field.set(factory, LocalDate.parse(hashMap.get(field.getName())));
                            } else {
                                field.set(factory, hashMap.get(field.getName()));
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
                if (!hasAnnotations) {
                    try {
                        if (field.getType() == List.class) {
                            field.set(factory, departments);
                        } else {
                            if (hashMap.get(field.getName()) == null) {
                                field.set(factory, null);
                            } else {
                                if (field.getType() == Long.class) {
                                    field.set(factory, Long.parseLong(hashMap.get(field.getName())));
                                }
                                if (field.getType() == String.class) {
                                    field.set(factory, hashMap.get(field.getName()));
                                }
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            });
            if (errors.size() > 0) {
                throw new FactoryParsingException("Ошибки валидации", errors);
            } else
                return factory;
        } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


}
